#!/bin/bash
export PYTHONIOENCODING=utf8
#source ~/.profile
pyenv local 3.6.5
git pull
#git submodule update
#git submodule foreach git pull origin master
cp template.txt restart.txt
while [ -f restart.txt ]
do
    git pull
    git submodule foreach git pull origin master
    python3 -m pip install --upgrade pip
    python3 -m pip install  --upgrade -r requirements.txt
    python3 bot.py
done