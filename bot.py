import asyncio
import discord
from discord.ext import commands
import traceback
import config
import sys
import os
import aiofiles
import random
#import uvloop
import random

def get_prefix(bot, message):
    """A callable Prefix for our bot. This could be edited to allow per server prefixes."""
    
    # Notice how you can use spaces in prefixes. Try to keep them simple though.
    prefixes = ['>']

    # If we are in a guild, we allow for the user to mention us or use any of the prefixes in our list.
    return commands.when_mentioned_or(*prefixes)(bot, message)

# Below cogs represents our folder our cogs are in. Following is the file name. So 'meme.py' in cogs, would be cogs.meme
# Think of it like a dot path import
initial_extensions = [
    'cogs.dl',
    'cogs.debugger',
    'cogs.owner',
    'cogs.util',
    'cogs.char',
    'cogs.random',

]   
bot = commands.Bot(command_prefix=get_prefix,owner_id=124316478978785283)

@bot.event
async def on_ready():
    print('Logged in as: '+ bot.user.name)
    print ('ID: ' +str(bot.user.id))
    print('------')
    if __name__ == '__main__':
        for extension in initial_extensions:
            try:
                bot.load_extension(extension)
            except Exception as e:
                print('Failed to load extension'+str(extension))
                traceback.print_exc()
    async with aiofiles.open('restart.txt') as file:
        data = await file.read()
        data = data.split('\n')
        channel = bot.get_channel(int(data[0]))
        user = bot.get_user(int(data[1]))
        await channel.send(f'{user.mention} WAAAAA')
        os.remove("restart.txt")

    #await bot.wait_until_ready()
    '''
    channel = bot.get_channel(config.logchannel)
    logembed=discord.Embed(title='I joined\t' + guild.name, color=0x00ff00)
    await channel.send(embed=logembed)
    headers = {'Authorization': config.STAT_TOKEN}
    api_url = 'https://discordbots.org/api/bots/340354052715970563/stats'
    shard_count = bot.shard_count
    guild_count = len(bot.guilds)
    data = {'server_count': guild_count,'shard_count': shard_count}
    async with aiohttp.ClientSession() as session:
        channel = bot.get_channel(config.logchannel)
        FMT = '%m/%d/%Y'
        now = datetime.now().strftime(FMT)
        await bot.wait_until_ready()
        await session.post(api_url, data=data, headers=headers)
        embed=discord.Embed(title='Posted '+ str(guild_count) + ' servers to discordbots.org', color=0x7287d9)
        await channel.send(embed=embed)
    '''



async def change_playing():
    await bot.wait_until_ready()
    l = [
        'With Smash Ultimate invitations',
        'Forward aerial',
        'WAAAAA',
        'Type >help',
    ]
    while 1:
        game = discord.Game(name=random.choice(l),activity=random.randint(0,3))
        await bot.change_presence(status=discord.Status.online,activity=game)
        await asyncio.sleep(300) # 5 minutes?
        
@bot.command()
async def ping(ctx):
    png = str(round(bot.latency * 1000,2))
    return await ctx.send("Pong! `"+png+'ms` :ping_pong:')

#########################################################  BELOW THIS LINE IS INIT ######################################################### 
bot.loop.create_task(change_playing())
bot.run(config.TOKEN, bot=True)