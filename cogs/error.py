import discord
from discord.ext import commands
import traceback
from config import ERROR_CH as cnl
import sys
import io


class ErrorCog():
    def __init__(self, bot):
        self.bot = bot
        self.channel = bot.get_channel(cnl)

    async def on_command_error(self, ctx, error):
        if isinstance(error, commands.errors.CommandNotFound):
            pass
        elif isinstance(error, discord.errors.Forbidden):
            pass
        elif isinstance(error, commands.errors.CheckFailure):
            await ctx.send("You don't have permissions for that")
        elif isinstance(error, commands.errors.MissingRequiredArgument):
            try:
                help = await self.bot.formatter.format_help_for(ctx, ctx.command,sendhelp=False)
                await ctx.send("You are missing a required argument"+"\n" + help[0])
            except Exception as error:
                trace = traceback.format_exception(type(error), error, error.__traceback__)
                out = '```This was sent from within MissingRequiredArgument handler\n'
                for i in trace:
                    if len(out+i+'```') > 2000:
                        await self.channel.send(out+'```')
                        out = '```'
                    out += i
                await self.channel.send(out+'```')
        elif isinstance(error, commands.errors.BadArgument):
            await ctx.send("Bad Argument")
        else:
            await ctx.send("An error occurred with the `{}` command. This has automatically been reported for you.".format(ctx.command.name))
            print("Ignoring exception in command {}".format(ctx.command.name))
            trace = traceback.format_exception(type(error), error, error.__traceback__)
            out = '```'
            for i in trace:
                if len(out+i+'```') > 2000:
                    await self.channel.send(out+'```')
                    out = '```'
                out += i
            await self.channel.send(out+'```')
            await self.channel.send(f"{ctx.author.id}: {ctx.message.content}")
    @commands.command(hidden=True)
    async def errorme(self,ctx):
        raise Exception

def setup(bot):
    bot.add_cog(ErrorCog(bot))