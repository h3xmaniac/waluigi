import discord
from discord.ext import commands
import asyncio
from random import shuffle, choice
from cogs.lib import uni

class Random():

        def __init__(self, bot):
            self.bot = bot


        @commands.command(aliases=["rc","rchar","randomchar","rcharacter","randomc"])
        async def randomcharacter(self, ctx):
            await ctx.send(f"Your random character is **{choice(uni.characters)}**")

        @commands.command(aliases=["rstage","rs","randoms"])
        async def randomstage(self, ctx):
             await ctx.send(f"Your random stage is **{choice(uni.stages)}**")


def setup(bot):
    bot.add_cog(Random(bot))