from discord.ext import commands
import asyncio
import discord
import subprocess
import os
import psutil
from cogs.lib import static
pid = os.getpid()
from cogs.lib import db
import subprocess
cur = db.cur()
import aiosqlite
class Util():

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def about(self,ctx):
        '''
        Shows bot information
        '''
        mem = int(psutil.Process(int(os.getpid())).memory_percent())
        cpu = int(psutil.Process(int(os.getpid())).cpu_percent())
        owner = self.bot.get_user(self.bot.owner_id)
        myKernel =  str(subprocess.Popen("uname -r", shell=True, stdout=subprocess.PIPE).communicate()[0].decode('utf-8').strip())
        uptime = str(subprocess.Popen("uptime --pretty | cut -c 3-", shell=True, stdout=subprocess.PIPE).communicate()[0].decode('utf-8').strip())
        png = str(round(self.bot.latency * 1000,2))
        cmt =  str(subprocess.Popen("cd /root/waluigi && git rev-parse --short=6 HEAD", shell=True, stdout=subprocess.PIPE).communicate()[0].decode('utf-8').strip())
        birthday = str(subprocess.Popen("cd /root/waluigi && git show -s --format=%cr 0b1b2c3d9d27f7a6132dbfb23300cf94a1218915", shell=True, stdout=subprocess.PIPE).communicate()[0].decode('utf-8').strip())
        #profiles = len(await cur.execute('SELECT did FROM profile').fetchall()) FFFFFFFFFFFFFUCK
        embed=discord.Embed(title="Waluigi stats",color=0x9954e9)
        embed.set_author(name=str(owner),url='http://spykebot.ink', icon_url=owner.avatar_url)
        #embed.add_field(name="Number of shards:", value=bot.shard_count, inline=True)
        embed.add_field(name="Version:", value=static.v, inline=True)
        embed.add_field(name="Commit:", value=cmt, inline=True)
        #embed.add_field(name="Shards:", value=str(self.bot.shard_count), inline=True)
        #embed.add_field(name="Shard ID:", value=str(ctx.guild.shard_id), inline=True)
        embed.add_field(name="I was created:", value=birthday, inline=True)
        embed.add_field(name="API:", value='discord.py rewrite '+discord.__version__, inline=True)
        embed.add_field(name="Database driver:", value='aiosqlite '+aiosqlite.__version__, inline=True)
        embed.add_field(name="Linux kernel version:", value=myKernel, inline=True)
        embed.add_field(name="PID:", value=str(pid), inline=True)
        embed.add_field(name="CPU usage:", value=str(cpu)+'%', inline=True)
        embed.add_field(name="Memory usage:", value=str(mem)+'%', inline=True)
        embed.add_field(name="Heartbeat ping:", value=png +' ms', inline=True)
        embed.add_field(name="Servers:", value=str(len(self.bot.guilds)), inline=True)
        #embed.add_field(name="Profiles:", value=profiles, inline=True) ok sure THATS FINE I GUESS
        #embed.add_field(name="Profile picture by:",value="[Chromsmith](http://chromsmith.tumblr.com/)",inline=True)
        #embed.add_field(name='Links:',value='[Invite Link](https://discordbots.org/bot/340354052715970563)\n[Patreon](https://www.patreon.com/user?u=8989413)\n[Support server](https://discord.gg/q2j8HtB)\n[Suggestions](http://freesuggestionbox.com/pub/lmjtkqd)\n[Documentation](http://spykebot.ink)\n[Profile documentation](https://docs.google.com/spreadsheets/d/1ZTQOauH6pPxjJFg3PzKr9otwvvdMhUOBna7VB1u7NG4/edit#gid=0)\n[Help translate Spyke](https://github.com/0xicl33n/spyke-locale)')
        #embed.add_field(name="Donate:", value='[Paypal](http://paypal.me/oxicl33n)', inline=True)
        #embed.add_field(name="Source:", value='[Gitlab](https://gitlab.com/h3xmaniac/waluigi)', inline=True)
        embed.set_footer(text="Uptime: "+uptime)
        await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(Util(bot))