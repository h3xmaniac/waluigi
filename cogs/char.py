from discord.ext import commands
import asyncio
import discord
from fuzzywuzzy import process
from ast import literal_eval as le

class Character():

    def __init__(self, bot):
        self.bot = bot
        with open("cogs/lib/character_stats.txt","r",encoding = 'utf8') as tmp:
            self.pdata = [[j.strip() for j in i.split("|")] for i in tmp.readlines()]
        with open("cogs/lib/fighterinfo.txt","r",encoding="utf-8") as tmp:
            self.sdata = le(tmp.read())
        with open("cogs/lib/framedata.txt","r",encoding = "utf-8") as tmp:
            self.fdata = le(tmp.read())
        self.cheadings = ['Weight', 'Max Jumps', 'Run Speed', 'Wall Jump', ' Walk Speed', 'Wall Cling', 'Air Speed', 'Crawl', 'Fall Speed', 'Tether', 'Fast Fall Speed', 'Jumpsquat', 'Air Acceleration', 'Soft Landing Lag', 'Gravity', 'Hard Landing Lag', 'SH Air Time', 'FH Air Time']
            

    @commands.command(pass_context=True,aliases=["fdata","frames"])
    async def framedata(self, ctx, game, *, character):
       
        game = game.casefold()
        ssb4 = ["ssb4","sm4sh","4"]
        ssb5 = ["5mash","sma5h","ssb5","5"]
        if game in ssb5:
            return await ctx.send("WIP")
        elif game in ssb4:
            if character not in self.fdata:
                char = process.extractOne(character, self.fdata.keys())
                if char[1] < 75:
                    return await ctx.send("That's not a character")
                character = char[0]
            data = self.fdata[character]
            c = character.replace(" ","_").casefold()
            f = discord.File(f"cogs/lib/img/char_icons/{c}.png", filename="char.png")
            titles = ["Standard Attacks","Misc. Attacks","Aerial Attacks","Special Attacks"]
            """
            if not move:
                list_of_moves = {}
                   for x in [z for z in titles if z!="Special Attacks"]:
                    for y in data[x]:
                        list_of_moves[y.lower()] = "\n".join([f"**{z}**: {data[x][y][z]}" for z in data[x][y]])
                move_match = {x: list_of_moves[x] for x in list_of_moves if move.lower() in x}
                if not move_match:
                    return await ctx.send("Not a move")
                embed = discord.Embed(title=f"Framedata for {character}")
                for x in move_match:
                    embed.add_field(name=x,value=move_match[x])
                return await ctx.send(embed=embed) 
            """
            embeds = {}
            for x in titles:
                embeds[x] = discord.Embed(title=x.rstrip("s") + f" framedata for {character}")
                embeds[x].set_thumbnail(url="attachment://char.png")
            for x in titles:
                for y in data[x]:
                    value = "\n".join([f"**{z}**: {data[x][y][z]}" for z in data[x][y]])
                    embeds[x].add_field(name=y,value=value)
            embed_list = [embeds[x] for x in embeds]
            home, back, forward, end = '⏮', '◀', '▶', '⏭'
            stop = '⏹'
            valid_r = [home,back,forward,end,stop]
            page = 0
            max_page = len(embed_list)
            msg = await ctx.send(embed=embed_list[page])
            for i in valid_r:
                await msg.add_reaction(i)
            await asyncio.sleep(0.1)
            def check(reaction,user):
                return reaction.emoji in valid_r and reaction.message.id == msg.id
            try:
                while True:
                    reaction, user = await self.bot.wait_for('reaction_add',check=check,timeout=120)
                    try:
                        await msg.remove_reaction(reaction, user)
                    except:
                        pass
                    if reaction.emoji == home:
                        page = 0
                    elif reaction.emoji == back:
                        page -= 1
                    elif reaction.emoji == forward:
                        page += 1
                    elif reaction.emoji == end:
                        page = max_page - 1
                    elif reaction.emoji == stop:
                        break

                    page %= max_page
                    await msg.edit(embed=embed_list[page])
            except:
                pass
            await msg.delete()





    @commands.command(pass_context=True,aliases=["charinfo","cinfo",])
    async def characterinfo(self, ctx, game, *, character):
        game = game.casefold()
        ssb4 = ["ssb4","sm4sh","4"]
        ssb5 = ["5mash","sma5h","ssb5","5"]
        if game in ssb5:
            return await ctx.send("WIP")
        elif game in ssb4:
            if character not in self.sdata:
                char = process.extractOne(character, self.sdata.keys())
                if char[1] < 75:
                    return await ctx.send("That's not a character")
                character = char[0]
            data = self.sdata[character]
            c = character.replace(" ","_").casefold()
            f = discord.File(f"cogs/lib/img/char_icons/{c}.png", filename="char.png")
            embed = discord.Embed(title=f"Smash 4 fighter information for {character}")
            embed.set_thumbnail(url="attachment://char.png")
            for x in data.keys():
                if x == "Grab":
                    continue
                value = ""
                if data[x][0]:
                    value = "{}\n".format(data[x][0])
                value+="{}".format(" | ".join(data[x][1]))
                embed.add_field(name=x,value=value,inline=True)
            await ctx.send(file=f, embed=embed)
                

    @commands.command(pass_context=True,aliases=["characterproperties","cproperty","statistics","stats","cstatistics","characterstatistics","cstats",])
    async def properties(self, ctx, game, *, character):
        game = game.casefold()
        ssb4 = ["ssb4","sm4sh","4"]
        ssb5 = ["5mash","sma5h","ssb5","5"]
        if game in ssb5:
            return await ctx.send("WIP")
        elif game in ssb4:
            if character not in [i[0] for i in self.pdata]:
                char = process.extractOne(character,[i[0] for i in self.pdata])
                if char[1] < 75:
                    return await ctx.send("That's not a character")
                character = char[0]
            match = [i for i in self.pdata if i[0] == character][0]
            c = character.replace(' ', '_').casefold()
            f = discord.File(f"cogs/lib/img/char_icons/{c}.png", filename="char.png")
            embed = discord.Embed(title=f'Smash 4 character stats for {character}')
            embed.set_thumbnail(url="attachment://char.png")
            for x in range(1,len(match)):
                embed.add_field(name=self.cheadings[x-1],value=match[x])
            await ctx.send(file=f, embed=embed)


def setup(bot):
    bot.add_cog(Character(bot))