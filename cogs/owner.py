from discord.ext import commands
from subprocess import run, PIPE
import discord, aiofiles, traceback
import importlib as imp
from discord import Colour, Embed
#from lib import dbutil
#codes = dbutil.codes
#splatnet = dbutil.splatnet
from cogs.lib import checks
#from config import error_channel as cnl
#db = dbutil.db
#from lib import uni
class OwnerCog:

    def __init__(self, bot):

        self.bot = bot
        self.channel = bot.get_channel(487359470398210088)
        self.lastdevdm = {}


    '''async def grabId(self,did):
        try:
            q = await codes.find_one({"discordId": did})
            itm = q['_id']
        except:
            return False
        if itm:
            return itm
        else:
            return False

    async def addDb(self,did,key,value):
        itm = await self.grabId(did)
        if itm:
            await codes.update_one({"_id" : itm } , {"$set": { key : value}})
            return True
        else:
            return False

    @commands.command(aliases=["dm"])
    @checks._check()
    async def directmessage(self, ctx, did: int, *, message):
        user = self.bot.get_user(did)
        if user:
            class Bunch:
                def __init__(self, **kwds):
                    self.__dict__.update(kwds)
            newctx = Bunch()
            newctx.author = user
            await user.send((await uni.translate(newctx,'misc.devdm')).format(message,ctx.author))
            #await user.send(f'{message}\n\n*Sent by `{ctx.author}`*\nYou can reply to them with the `reply` command executed in this DM')
            self.lastdevdm[did] = ctx.author
            await ctx.send(f'Messaged {self.bot.get_user(did)} successfully')
    @commands.command(aliases=['reply'])
    async def respond(self,ctx,*,message):
        if self.lastdevdm[ctx.author.id] and not ctx.guild:
            await self.lastdevdm[ctx.author.id].send(f'{ctx.author} ({ctx.author.id}) has replied to your DM:\n\n{message}')
            await ctx.send((await uni.translate(ctx,'misc.devreply')).format(self.lastdevdm[ctx.author.id]))
    # Hidden means it won't show up on the default help.'''
    @commands.command(name='load')
    @checks._check()
    #@commands.is_owner()
    async def cog_load(self, ctx, *cogs):
        """Command which Loads a Module.
        Remember to use dot path. e.g: cogs.owner"""
        for cog in cogs:
            try:
                try:
                    self.bot.load_extension(cog)
                except ModuleNotFoundError:
                    cog1 = 'cogs.' + cog
                    try:
                        self.bot.load_extension(cog1)
                    except Exception as e:
                        embed = Embed(colour=Colour(0xff0000))
                        embed.set_author(name="ERROR")
                        embed.add_field(name=type(e).__name__,value=e)
                        await ctx.send(embed=embed)
                    else:
                        embed = Embed(colour=Colour(0x00ff00))
                        embed.set_author(name="SUCCESS")
                        embed.add_field(name="Successfully loaded",value=cog1)
                        await ctx.send(embed=embed)
                except Exception as e:
                    embed = Embed(colour=Colour(0xff0000))
                    embed.set_author(name="ERROR")
                    embed.add_field(name=type(e).__name__,value=e)
                    await ctx.send(embed=embed)
                else:
                    embed = Embed(colour=Colour(0x00ff00))
                    embed.set_author(name="SUCCESS")
                    embed.add_field(name="Successfully loaded",value=cog)
                    await ctx.send(embed=embed)
            except Exception as e:
                trace = traceback.format_exception(type(e), e, e.__traceback__)
                out = '```'
                for i in trace:
                    if len(out+i+'```') > 2000:
                        await self.channel.send(out+'```')
                        out = '```'
                    out += i
                await self.channel.send(out+'```')

    @commands.command(name='unload')
    @checks._check()
    #@commands.is_owner()
    async def cog_unload(self, ctx, *cogs):
        """Command which Unloads a Module.
        Remember to use dot path. e.g: cogs.owner"""
        for cog in cogs:
            try:
                try:
                    self.bot.unload_extension(cog)
                except ModuleNotFoundError:
                    cog1 = 'cogs.' + cog
                    try:
                        self.bot.unload_extension(cog1)
                    except Exception as e:
                        embed = Embed(colour=Colour(0xff0000))
                        embed.set_author(name="ERROR")
                        embed.add_field(name=type(e).__name__,value=e)
                        await ctx.send(embed=embed)
                    else:
                        embed = Embed(colour=Colour(0x00ff00))
                        embed.set_author(name="SUCCESS")
                        embed.add_field(name="Successfully unloaded",value=cog1)
                        await ctx.send(embed=embed)
                except Exception as e:
                    embed = Embed(colour=Colour(0xff0000))
                    embed.set_author(name="ERROR")
                    embed.add_field(name=type(e).__name__,value=e)
                    await ctx.send(embed=embed)
                else:
                    embed = Embed(colour=Colour(0x00ff00))
                    embed.set_author(name="SUCCESS")
                    embed.add_field(name="Successfully unloaded",value=cog)
                    await ctx.send(embed=embed)
            except Exception as e:
                trace = traceback.format_exception(type(e), e, e.__traceback__)
                out = '```'
                for i in trace:
                    if len(out+i+'```') > 2000:
                        await self.channel.send(out+'```')
                        out = '```'
                    out += i
                await self.channel.send(out+'```')
    @commands.command(name="restart")
    @checks._check()
    #@commands.is_owner()
    async def bot_reload(self, ctx):
        async with aiofiles.open('restart.txt','w') as file:
            await file.write(f'{ctx.channel.id}\n{ctx.author.id}')
        await ctx.send('brb')
        await self.bot.logout()

    @commands.command(name='reload')
    @checks._check()
    #@commands.is_owner()
    async def cog_reload(self, ctx, *cogs):
        """Command which Reloads a Module.
        Remember to use dot path. e.g: cogs.owner"""
        res = False
        for cog in cogs:
            if cog == 'all':
                res = True
                break
            try:
                try:
                    if cog == 'profile':
                        raise ModuleNotFoundError # profile is a thing already apparently
                    self.bot.unload_extension(cog)
                    self.bot.load_extension(cog)
                except ModuleNotFoundError:
                    cog1 = 'cogs.' + cog
                    try:
                        self.bot.unload_extension(cog1)
                        self.bot.load_extension(cog1)
                    except ModuleNotFoundError:
                        cog = 'cogs.lib.'+cog
                        try:
                            mod = imp.import_module(cog)
                            imp.reload(mod)
                            embed = Embed(colour=Colour(0x00ff00))
                        except Exception as e:
                            embed = Embed(colour=Colour(0xff0000))
                            embed.set_author(name="ERROR")
                            embed.add_field(name=type(e).__name__,value=e)
                            await ctx.send(embed=embed)
                        else:
                            embed = Embed(colour=Colour(0x00ff00))
                            embed.set_author(name="SUCCESS")
                            embed.add_field(name="Successfully reloaded",value=cog)
                            await ctx.send(embed=embed)
                    except Exception as e:
                        embed = Embed(colour=Colour(0xff0000))
                        embed.set_author(name="ERROR")
                        embed.add_field(name=type(e).__name__,value=e)
                        await ctx.send(embed=embed)
                    else:
                        embed = Embed(colour=Colour(0x00ff00))
                        embed.set_author(name="SUCCESS")
                        embed.add_field(name="Successfully reloaded",value=cog1)
                        await ctx.send(embed=embed)
                except Exception as e:
                    if type(e).__name__ == 'ClientException' and str(e) == 'extension does not have a setup function':
                        mod = imp.import_module(cog)
                        imp.reload(mod)
                        embed = Embed(colour=Colour(0x00ff00))
                        embed.set_author(name="SUCCESS")
                        embed.add_field(name="Successfully reloaded",value=cog)
                        await ctx.send(embed=embed)
                    else:
                        embed = Embed(colour=Colour(0xff0000))
                        embed.set_author(name="ERROR")
                        embed.add_field(name=type(e).__name__,value=e)
                        await ctx.send(embed=embed)
                else:
                    embed = Embed(colour=Colour(0x00ff00))
                    embed.set_author(name="SUCCESS")
                    embed.add_field(name="Successfully reloaded",value=cog)
                    await ctx.send(embed=embed)
            except Exception as e:
                trace = traceback.format_exception(type(e), e, e.__traceback__)
                out = '```'
                for i in trace:
                    if len(out+i+'```') > 2000:
                        await self.channel.send(out+'```')
                        out = '```'
                    out += i
                await self.channel.send(out+'```')
        if res:
            ctx.command = self.bot_reload
            await ctx.reinvoke()

    @commands.command(name="stop")
    @checks._check()
    #@commands.is_owner()
    async def bot_unload(self, ctx):
        await self.bot.logout()
    @commands.command()
    @checks._check()
    #@commands.is_owner()
    async def nextpls(self, ctx):
        await ctx.send('Another satisfied customer. Next please!')

    @commands.command(name="update")
    @checks._check()
    #@commands.is_owner()
    async def bot_update(self, ctx, *cogs):
        await ctx.send("```"+run(["git", "pull",], stdout=PIPE,encoding="ASCII").stdout+"```")
        await ctx.send("```"+run(["git","submodule","foreach","git","pull","origin","master"], stdout=PIPE,encoding="ASCII").stdout+"```")
        if cogs:
            ctx.command = self.cog_reload
            await ctx.reinvoke()

    '''@commands.command()
    @checks._check()
    #@commands.is_owner()
    async def debugdb(self,ctx,did,key,value):
        if await self.addDb(did,key,value):
            return await ctx.send('Key/value pair for: `'+did + '` set `' + key +"` : `"+ value+"`" )

    @commands.command()
    @checks._check()
    #@commands.is_owner()
    async def massmessage(self,ctx,col,*,message):

        cols = {
            'splatnet' : db.splatnet.distinct,
            'salmon' : db.salmon.distinct,
            'maps' : db.maps.distinct,
            'lfg' : db.lfg.distinct 
            }
        if col not in cols:
            await ctx.send('m8 u missin a col')
        else:
            for itm in await cols[col]('serverId'):
                try:
                    ch = self.bot.get_channel(itm)
                    await ch.send(message)
                except (discord.errors.Forbidden, AttributeError,TimeoutError):
                    pass
            await ctx.send('Massmessage sent.')'''
    @commands.command()
    @checks._check()
    async def run(self,ctx,*cmd):
        await ctx.send("```"+run(cmd, stdout=PIPE,encoding="ASCII").stdout+"```")

    @commands.command()
    @checks._check()
    async def prefixdebug(self,ctx,guild_id : int, prefix : str):
        self.bot.prefixes[guild_id] = prefix
        if prefix == '': del self.bot.prefixes[guild_id]
        with open('prefixes.txt','w') as file:
            file.write(repr(self.bot.prefixes))
        guild = self.bot.get_guild(guild_id)
        await ctx.send(f'Set prefix for {guild.name if guild else "[INVALID SERVER]"} to `{prefix}`')
    @commands.command()
    @checks._check()
    async def addconfig(self,ctx,key,value):
        async with aiofiles.open('config.py','a') as file:
            file.write(f'\n{key} = \'{value}\'')
def setup(bot):
    bot.add_cog(OwnerCog(bot))