import discord
from discord.ext import commands
import aiosqlite
from cogs.lib import db
import i18n
i18n.load_path.append('cogs/lib/locale')
i18n.set('fallback','en')

v = 'v0.1-indev'

characters = ["Mario",
"Donkey Kong",
"Link",
"Samus",
"Dark Samus",
"Yoshi",
"Kirby",
"Fox",
"Pikachu",
"Luigi",
"Ness",
"Captain Falcon",
"Jigglypuff",
"Peach",
"Daisy",
"Bowser",
"Ice Climbers",
"Sheik",
"Zelda",
"Dr. Mario",
"Pichu",
"Falco",
"Marth",
"Lucina",
"Young Link",
"Ganondorf",
"Mewtwo",
"Roy",
"Chrom",
"Mr Game & Watch",
"Meta Knight",
"Pit",
"Dark Pit",
"Zero SuitSamus",
"Wario",
"Snake",
"Ike",
"Pokémon Trainer",
"Diddy Kong",
"Lucas",
"Sonic",
"King Dedede",
"Olimar",
"Lucario",
"R.O.B.",
"Toon Link",
"Wolf",
"Villager",
"Mega Mn",
"Wii Fit Trainer",
"Rosalina & Luma",
"Little Mac",
"Greninja",
"Mii Brawler",
"Mii Swordfighter",
"Mii Gunner",
"Palutena",
"Pac-Man",
"Robin",    
"Shulk",
"Bowser Jr.",
"Duck Hunt",
"Ryu",
"Cloud",
"Corrin",
"Bayonetta",
"Inkling",
"Ridley",
"Simon",
"Richter",
"King K. Rool"]

stages = ["Peach's Castle",
"Mushroom Kingdom",
"Princess Peach's Castle",
"Rainbow Cruise",
"Mushrom Kingdom II",
"Delfino Plaza",
"Luigi's Mansion",
"Mushroomy Kingdom",
"Figure-8 Circuit",
"Mario Bros.",
"3D Land",
"Golden Plains",
"Paper Mario",
"Mushroom Kingdom U",
"Mario Galaxy",
"Mario Circuit",
"Super Mario Maker",
"Super Happy Tree",
"Yoshi's Island (Melee)",
"Yoshi's Story",
"Yoshi's Island",
"Kongo Jungle",
"Kongo Falls",
"Jungle Japes",
"75m",
"Hyrule Castle",
"Great Bay",
"Temple",
"Bridge of Eldin",
"Pirate Ship",
"Geudo Valley",
"Spirit Train",
"Skyloft",
"Brinstar",
"Brinstar Depths",
"Norfair",
"Frigate Orpheon",
"Dream Land",
"Fountain of Dreams",
"Green Greens",
"Halberd",
"Dream Land GB",
"The Great Cave Offensive",
"Corneria",
"Venom",
"Lylat Cruise",
"Saffron City",
"Pokémon Stadium",
"Pokémon Stadium 2",
"Spear Pillar",
"Unova Pokémon League",
"Prism Tower",
"Kalos Pokémon League",
"Big Blue",
"Port Town Aero Drive",
"Mute City SNES",
"Onett",
"Fourside",
"New Pork City",
"Magicant",
"Summit",
"Castle Siege",
"Arena Ferox",
"Coliseum",
"Flat Zone X",
"SkyWorld",
"Reset Bomb Forest",
"Palutena's Temple",
"WarioWare Inc.",
"Gamer",
"Distant Planet",
"Garden of Hope",
"Smashville",
"Tortimer Island",
"Town and City",
"Boxing Ring",
"Wii Fit Studio",
"Gaur Plain",
"Duck Hunt",
"Shadow Moses Island",
"Green Hill Zone",
"Windy Hill Zone",
"Wily Castle",
"Pac-Land",
"Suzaku Castle",
"Midgar",
"Umbra Clock Tower",
"Hanenbow",
"Pictochat 2",
"Balloon Fight",
"Living Room",
"Find Mii",
"Tomodachi Life",
"Wrecking Crew",
"Pilotwings",
"Wuhu Island",
"Battlefield",
"Big Battlefield",
"Final Destination",
"New Donk City Hall",
"Great Plateau Tower",
"Moray Towers",
"Dracula's Castle"]

async def check(ctx,the_id,entry):
    async with aiosqlite.connect('weegee.db') as db:
        db.row_factory = db.dict_factory
        cur = await db.execute(f"SELECT {entry} FROM profile WHERE did = {the_id};")
        if cur.fetchone()[entry]:
            return cur.fetchone()[entry]
        else:
            return False

cache = {}

async def translate(ctx, transtr: str, over: str=None) -> str:
      if not over:
            global cache
            if ctx.author.id in cache:
                  locale = cache[ctx.author.id]
            elif await check(ctx,ctx.author.id,"locale"):
                  locale = await check(ctx,ctx.author.id,"locale")
            else:
                  locale = 'en'
      else:
            locale = over
      cache[ctx.author.id] = locale
      i18n.set('locale', locale)
      return i18n.t(transtr)

async def clear_cache(ctx):
      global cache
      if ctx.author.id in cache:
            del cache[ctx.author.id]
