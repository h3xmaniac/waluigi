from bs4 import BeautifulSoup as BS 
import aiohttp
import asyncio
from cogs.lib import static as uni

fighters = {}
characters = ['Mario', 'Luigi', 'Peach', 'Bowser', 'Dr. Mario', 'Yoshi', 'Donkey Kong', 'Diddy Kong', 'Link', 'Zelda', 'Sheik', 'Ganondorf', 'Toon Link', 'Samus', 'Zero Suit Samus', 'Kirby', 'Meta Knight', 'King Dedede', 'Fox', 'Falco', 'Pikachu', 'Jigglypuff', 'Mewtwo', 'Charizard', 'Lucario', 'Captain Falcon', 'Ness', 'Lucas', 'Marth', 'Roy', 'Ike', 'Mr. Game and Watch', 'Pit', 'Wario', 'Olimar', 'R.O.B', 'Sonic', 'Rosalina & Luma', 'Bowser Jr', 'Greninja', 'Robin', 'Lucina', 'Corrin', 'Palutena', 'Dark Pit', 'Villager', 'Little Mac', 'Wii Fit Trainer', 'Shulk', 'Duck Hunt', 'Mega Man', 'Pac-Man', 'Ryu', 'Cloud', 'Bayonetta', 'Mii Brawler', 'Mii Swordfighter', 'Mii Gunner', 'Waluigi']




async def getdata(character):
    character = character.replace(" ","_")
    async with aiohttp.ClientSession() as session:
        async with session.get(f"https://www.ssbwiki.com/{character}_(SSB4)") as resp:
            data = await resp.read()
    soup = BS(data.decode("utf-8"),"html.parser")
    table = [tabel for tabel in soup.find_all("table") if "Floor attack (front)" in str(tabel)][0]
    while True:
        try:
            table = [tabel for tabel in table.find_all("table") if "Floor attack (front)" in str(tabel)][0]
        except:
            break

    rows = table.find_all("tr")[1:]
    titles = ["Neutral Attack","Forward Tilt","Up Tilt","Down Tilt","Dash Attack",
        "Forward Smash","Up Smash","Down Smash",
        "Neutral Aerial","Forward Aerial","Back Aerial","Up Aerial","Down Aerial",
        "Grab","Pummel",
        "Forward Throw","Back Throw","Up Throw","Down Throw",
        "Floor Attack (Front)","Floor Attack (Back)","Floor Attack (Trip)",
        "Neutral Special","Side Special","Up Special","Down Special","Final Smash"]
    headers = {}
    moveset = {}
    index = 0
    for row in rows:
        index+=1
        if not row.find("th"):
            pretendindex = index-1
            while True:
                if rows[pretendindex].find("th"):
                    headers[rows[pretendindex].find("th").text.strip().title()].append(row)
                    break
                pretendindex-=1
            continue
        headers[row.find("th").text.strip().title()] = [row]
    directory = {}
    for x in titles:
        try:
            row = [headers[y] for y in headers if y == x][0]
        except:
            print(x)
            continue
        if "Special" in x:
            row = [row[0]]
        name = row[0].find_all("td")[-3].text.strip()
        if name == "\xa0" or name == "":
            name = None
        description = row[0].find_all("td")[-1].text.strip()
        damage = [[x.text.strip() for x in r.find_all("td") if "%" in x.text.strip() and x.text.strip() != description] for r in row]
        try:
            damage = [x[0] for x in damage]
        except:
            damage = ""
        directory[x] = [name,damage]
    fighters[character.replace("_"," ")] = directory

async def function():
    for char in characters:
        try:
            await getdata(char)
        except Exception as e:
            print(char)
            print(e)
    return fighters




        
        
        
    