import aiosqlite
import config
from cogs.lib import uni
def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d
async def getprof(did,ctx,fromlocale=False):
    async with aiosqlite.connect(config.db) as db:
        db.row_factory = dict_factory
        cur = await db.execute('SELECT * FROM `profile` WHERE did = ?;',[(did)])
        if cur:
            prof = await cur.fetchone()
            await cur.close()
            if prof and not fromlocale:
                for i in prof:
                    if prof[i] is None:
                        prof[i] = await uni.translate(ctx,'common.unset')
            return prof if prof else {'locale':'en'}
        else:
            return {'locale':'en'}
async def insert(did,k,v):
    if await getprof(did,None,fromlocale=True) == {'locale':'en'}:
        async with aiosqlite.connect(config.db) as db:
            await db.execute(f'INSERT OR REPLACE INTO `profile`(did,`{k}`) VALUES(?,?)',(did,v))
            await db.commit()
    else:
        async with aiosqlite.connect(config.db) as db:
            await db.execute(f'UPDATE `profile` SET `{k}` = ? WHERE did = {did};',[(v)])
            await db.commit()
            return True