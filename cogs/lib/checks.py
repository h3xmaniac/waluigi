from discord.ext import commands
def check_bot():
    async def predicate(ctx):
        return not ctx.author.bot
    return commands.check(predicate)

def _check():
    async def predicate(ctx):
        ids = [232948417087668235,124316478978785283,131131701148647424]
        return ctx.author.id in ids
    return commands.check(predicate)